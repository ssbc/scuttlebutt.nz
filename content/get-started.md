---
title: Get Started
type: page
---

# Get Started

Scuttlebutt is a social network and it works like others social apps we already use.
The key difference is that it is decentralised, meaning not centralised.

Therefore, it works by not one company or database holding all the information, but
it's spread out and held by all your friends, and each of our computers.

The easiest way to learn about Scuttlebutt is to dive right in.

## Step 1: Download

Download the app that suits you (see descriptions).

Each one allows you to access the same Scuttlebutt universe, just through different interfaces.
Just like how we can access the internet with different browsers.

Once you create a profile, all your data will be stored locally on your device.

For more information and support, visit the website for your chosen app's maintainer.


<div class="get-started-apps">
            <div class="col-4 col-s-4">
              <a href="https://github.com/ssbc/patchwork"><img
              src="/images/app-icons/hermiepatchwork.svg" alt="patchwork logo"></a>
            </div>
            <div class="col-8 col-s-8">
                <h1>Patchwork</h1>
                <h3>(Windows, MacOS, Linux)</h3>
                  <p>Standalone, beginner-friendly social view of Scuttlebutt.</p>
                  <p><strong>Recommended for first-timers.</strong></p>
                  <div style="height:30px">
                    <a href="http://dinosaur.is/patchwork-downloader/" class="button">Download</a>
                  </div>
            </div>
</div>

<div class="get-started-apps">
            <div class="col-4 col-s-4">
              <a href="https://github.com/ssbc/patchbay"><img
              src="/images/app-icons/hermiepatchbay2.svg" alt="patchbay logo"></a>
            </div>
            <div class="col-8 col-s-8">
                <h1>Patchbay</h1>
                <h3>(Windows, MacOS, Linux)</h3>
                  <p>Bleeding-edge, tab-based interface with experimental features.</p>
                  <div style="height:30px">
                    <a href="https://github.com/ssbc/patchbay/releases" class="button">Download</a>
                  </div>
            </div>
</div>

<div class="get-started-apps">
            <div class="col-4 col-s-4">
              <a href="https://www.manyver.se/"><img class="icon"
              src="/images/app-icons/manyverse.png" alt="manyverse logo"></a>
            </div>
            <div class="col-8 col-s-8">
                <h1>Manyverse</h1>
                <h3>(Android, iOS, Windows, MacOS, Linux)</h3>
                  <p>Mobile-first user-friendly app with modern features</p>
                  <div style="height:30px">
                    <a href="https://www.manyver.se/" class="button">Download</a>
                  </div>
            </div>
</div>

<div class="get-started-apps">
            <div class="col-4 col-s-4">
              <a href="https://github.com/soapdog/patchfox"><img class="icon"
              src="/images/app-icons/patchfox_128.png" alt="patchfox logo"></a>
            </div>
            <div class="col-8 col-s-8">
                <h1>Patchfox</h1>
                <h3>(Firefox Add-on)</h3>
                  <p>A new way to access Scuttlebutt packaged as a Web Extension for Firefox. Needs you to run sbot, or have Patchwork/Patchbay running.</p>
                  <div style="height:30px">
                    <a href="https://addons.mozilla.org/en-US/firefox/addon/patchfox/" class="button">Download</a>
                  </div>
            </div>
</div>

<div class="get-started-apps">
            <div class="col-4 col-s-4">
              <a href="https://planetary.social/"><img class="icon"
              src="/images/app-icons/planetary_logo.png" alt="planetary logo"></a>
            </div>
            <div class="col-8 col-s-8">
                <h1>Planetary</h1>
                <h3>(iOS)</h3>
                  <p>Native iOS scuttlebutt social feeds application with pre-loaded pubs.</p>
                  <div style="height:30px">
                    <a href="https://apps.apple.com/us/app/planetary-app/id1481617318" class="button">Download</a>
                  </div>
            </div>
</div>


These are currently the most popular apps, but there are many [more apps](https://www.scuttlebutt.nz/applications) to choose from!

&nbsp;

## Step 2: Boot it up

Whichever app you choose, open the application. Below, we'll guide you through the Patchwork app.

Start by setting up your profile. This might include choosing friendly picture for your profile, or writing a little about yourself.

You can change this all later. It's possible to discover your old names and photo and description, so consider the privacy implications of whatever you choose. You may want to start with a temporary-feeling identity and change it later once you settle in.

![patchwork at first bootup](/images/guide/patchwork-first-boot.png)

&nbsp;

## Step 3: Get Connected

Since Scuttlebutt is a decentralized, peer-to-peer network, it’s up to you to decide what content to download and share.

The easiest way to try it all out is to join an online Room; ask someone you know for a room invite! You can also follow friends directly by exchanging keys.

Go to the [list of Room servers](https://github.com/ssbc/ssb-server/wiki/%23ssbrooms), and get an invite code from one.

&nbsp;

### Use the Invite Code

The invite code is a special unique code, that allows you to be "invited" into a particular community or group of people. If you use Patchwork, can click “+ Join Server” in the top left corner of Patchwork and paste in the invite code.


&nbsp;

## Step 4: Get the Gossip

After connecting to a room you'll be able to connect directly to other people that have connected to the same room. Depending on who you "follow" after connecting there might be some downloading, sorting and processing. This **“inital syncing"** process can take up to an hour and use a fair amount of data.

It’s a good time to go have a cup of tea or to watch the [Scuttlebutt Love Story](/docs/introduction).

![Initial Sync Island](/images/guide/initial-sync-island.png)

&nbsp;

## Step 5: Get Social!

Woo yay! Congrats! You've made it!

We have a tradition that new people write introductions in the `#new-people` channel. You can also now follow new people you find, 'like' discussions, private message people and follow channels!

Check out the detailed [Get Started Guide](/docs/introduction/detailed-start) for more info.

&nbsp;

![hermie gifting you and welcoming you with a flower](/images/gif/hermies-gift.gif)

# Scuttle site in hugo

## quick start

### install hugo

see hugo's [installation instructions](https://gohugo.io/getting-started/installing/).

### checkout missing git submodules

The hugo's book template is linked as a git submodule. The submodule is not automatically checkout out when cloning this repository.

```
git submodule init
git submodule update
```

### run

```
hugo server
```

### build

```
hugo
```

Output will be in ./public/ directory

## Directory Structure

We're using the standard hugo directory structure using content pages.

```
├─ content/
|  └ about/ # About page
├ layouts/ # You can add extra layout files here. 'About page' currently follows the page/single.html template
├ static/ # Your static files are in this directory, like images and css
├ .gitignore
├ .gitmodules
├ config.toml # Hugo config file containing general settings and menu configs.
```

## to do

- global styling and design- break up into scss?

- [ ] external blogs/articles/media

- ecosystem page
  - [ ] list of initiatives and "sister projects"

- docs page
  - [ ] command line set up
  - [ ] links to tutorials and modules
  - [ ] learn about the different applications

- posts page setup
  - [ ] community blog setup, D&I, ssb camp, dweb
  - [ ] past events

- research page
  - [ ] info and link to diff research

- grant funding page
  - [ ] transparency on where money comes from & goes?

- [ ] organise content with multiple languages
    - [ ] chinese
    - [ ] spanish
    - [ ] french
    - [ ] italian
    - [ ] portugese
    - [ ] dutch

# done

- hugo setup
  - home page setup
    - [x] inital setup
    - [x] refine copy & global css designs
- about page
  - [x] initial set up with config main menu
  - [x] works with markdown `content`
  - [x] styling for markdown content
  - [x] community agreed description of what ssb is/ethos/principles
  - [x] ssb love story
  - [x] principles

- nav bar
  - [x] initial setup, configurable via `config.toml` & `header.html`
  - [x] multi-language friendly
  - [x] sets up about and docs pages

- multi-language initial setup, for variables, in i18n folder and config.toml

- research page
  - [x] initial setup

- docs page
  - [x] quick start downloading from easy 'get started button'
  - [x] learn about the protocol

- netifly deployment
